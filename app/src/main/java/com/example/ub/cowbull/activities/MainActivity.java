package com.example.ub.cowbull.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.Switch;
import android.widget.Toast;

import com.example.ub.cowbull.R;
import com.example.ub.cowbull.fragments.DetailFragment;
import com.example.ub.cowbull.fragments.PlayFragment;


public class MainActivity extends FragmentActivity {

    View view;
    DrawerLayout drawerLayout;
    ListView drawerList;
    String[] menu;
    ArrayAdapter<String> slidingMenuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = (DrawerLayout)this.findViewById(R.id.drawer_layout);
        drawerList = (ListView)this.findViewById(R.id.left_drawer);
        menu = getResources().getStringArray(R.array.slide_menu_items);
        slidingMenuAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,menu);
        drawerList.setAdapter(slidingMenuAdapter);
        drawerList.setSelector(android.R.color.holo_blue_dark);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new PlayFragment()).commit();
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            Fragment displayFragment = new PlayFragment();;
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
                drawerLayout.closeDrawers();
                Bundle args = new Bundle();
                args.putString("Menu", menu[position]);
                switch (position){
                    case 0: break;
                    case 1: displayFragment = new PlayFragment();
                        break;
                    case 2: break;
                    case 3: break;
                    case 4: exitActivity();
                }
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, displayFragment).commit();
            }
        });
    }

    public void exitActivity(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.exit_confirmation);

        // Setting Icon to Dialog
//                    alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view;
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
