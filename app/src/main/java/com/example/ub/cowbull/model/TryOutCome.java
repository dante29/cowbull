package com.example.ub.cowbull.model;

/**
 * Created by ub on 14/12/14.
 */
public class TryOutCome {

    private int tryNumber;
    private String userInput;
    private String outCome;

    public TryOutCome(int tryNumber, String userInput, String outCome) {
        this.tryNumber = tryNumber;
        this.userInput = userInput;
        this.outCome = outCome;
    }

    public TryOutCome() {
    }

    public int getTryNumber() {
        return tryNumber;
    }

    public void setTryNumber(int tryNumber) {
        this.tryNumber = tryNumber;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    public String getOutCome() {
        return outCome;
    }

    public void setOutCome(String outCome) {
        this.outCome = outCome;
    }
}
