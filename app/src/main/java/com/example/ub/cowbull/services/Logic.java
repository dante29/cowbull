package com.example.ub.cowbull.services;

import com.example.ub.cowbull.Constants;
import com.example.ub.cowbull.model.Result;

/**
 * Created by ub on 14/12/14.
 */
public class Logic {

    private static String chosenNumber;

    public Logic(){}

    public Logic(String chosenNumber){
        this.chosenNumber = chosenNumber;
    }
    public Result returnAnswer(String answer){
        int length = answer.length();
        int count = 0;
        for(int i=0; i<length; i++){
            if(answer.charAt(i) == chosenNumber.charAt(i))
                count++;
        }
        return new Result();
    }
}
