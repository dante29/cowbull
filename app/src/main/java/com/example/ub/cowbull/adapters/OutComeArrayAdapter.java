package com.example.ub.cowbull.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ub.cowbull.R;
import com.example.ub.cowbull.model.TryOutCome;

import java.util.List;

/**
 * Created by ub on 14/12/14.
 */
public class OutComeArrayAdapter extends ArrayAdapter<TryOutCome>{

    private List<TryOutCome> values;
    private Context context;
    private int resource;

    public OutComeArrayAdapter(Context context, int resource, List<TryOutCome> values){
        super(context, resource, values);
        this.context = context;
        this.values = values;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(resource, parent, false);
        TextView tryNumberValue = (TextView)rowView.findViewById(R.id.try_number_value);
        TextView outcomeValue = (TextView)rowView.findViewById(R.id.outcome_value);
        TextView yourInputValue = (TextView)rowView.findViewById(R.id.your_input_value);
        TryOutCome outcome = values.get(position);
        tryNumberValue.setText(new Integer(outcome.getTryNumber()).toString());
        outcomeValue.setText(outcome.getOutCome());
        yourInputValue.setText(outcome.getUserInput());

        return rowView;
    }

    public void addValues(TryOutCome tryOutCome){
        this.values.add(tryOutCome);
    }
}
