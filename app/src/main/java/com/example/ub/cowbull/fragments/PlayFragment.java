package com.example.ub.cowbull.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ub.cowbull.Constants;
import com.example.ub.cowbull.R;
import com.example.ub.cowbull.adapters.OutComeArrayAdapter;
import com.example.ub.cowbull.model.Result;
import com.example.ub.cowbull.model.TryOutCome;
import com.example.ub.cowbull.services.Logic;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PlayFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PlayFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlayFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View view;
    private Button submit_button;
    private TextView tries_left;
    private EditText inputText;
    private TextView tryNumberValue;
    private TextView outcomeValue;
    private TextView yourInputValue;
    private ListView outcomeListView;

    AlertDialog.Builder alertDialog;
    Resources resources;
    private int inputLength;
    private int tries;
    private int max_tries;
    private int inputValue;
    private Result result;
    private int numberToBeGuessed = 1111;
    private boolean isNewGame = true;
    Logic logic;
    ArrayList<TryOutCome> outcomeList;
    OutComeArrayAdapter outComeArrayAdapter;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PlayFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PlayFragment newInstance(String param1, String param2) {
        PlayFragment fragment = new PlayFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(isNewGame){
            isNewGame = false;
            numberToBeGuessed = getNumberToBeGuessed();
            logic = new Logic(Integer.toString(numberToBeGuessed));
        }
        if(view==null) {
            resources = this.getActivity().getResources();
            inputLength = resources.getInteger(R.integer.input_size);
            view = inflater.inflate(R.layout.fragment_play, container, false);
            submit_button = (Button) view.findViewById(R.id.submit_button);
            tries_left = (TextView) view.findViewById(R.id.tries_left);
            max_tries = resources.getInteger(R.integer.max_tries);
            tries = max_tries;
            tries_left.setText(new Integer(tries).toString());
            inputText = (EditText) view.findViewById(R.id.input);

            outcomeListView = (ListView)view.findViewById(R.id.outcome_listview);
            outcomeList = new ArrayList<TryOutCome>();
            outComeArrayAdapter = new OutComeArrayAdapter(this.getActivity(),R.layout.outcomes_listview, outcomeList);
            outcomeListView.setAdapter(outComeArrayAdapter);
            addListenerOnButton();
        }
        alertDialog = new AlertDialog.Builder(this.getActivity());

        // Setting Dialog Title
        alertDialog.setTitle(resources.getString(R.string.game_result_alert_box_title));
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void addListenerOnButton() {

        submit_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                inputValue = Integer.parseInt(inputText.getText().toString());
                result = logic.returnAnswer(Integer.toString(inputValue));
                String userInputValue = new Integer(inputValue).toString();
                inputText.setText("");
                int bulls = result.getBulls();
                int cows = result.getCows();
                String outcome = bulls + " " + Constants.BULLS + " & " + cows + " " + Constants.COWS;
                TryOutCome tryOutCome = new TryOutCome(max_tries - tries + 1, userInputValue, outcome);
                outComeArrayAdapter.add(tryOutCome);
//                outComeArrayAdapter.notifyDataSetChanged();
                tries--;
                tries_left.setText(new Integer(tries).toString());

                if(tries == 0){

                    // Setting Dialog Message
                    alertDialog.setMessage(resources.getString(R.string.game_result_alert_loose_message));

                    // Setting Icon to Dialog
//                    alertDialog.setIcon(R.drawable.delete);

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton(resources.getString(R.string.game_result_alert_positive_button_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.cancel();
                            resetGame();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton(R.string.game_result_alert_negative_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            //TODO:
                            Toast.makeText(getActivity().getApplicationContext(), "EXIT GAME", Toast.LENGTH_LONG).show();
                        }
                    });
                    alertDialog.show();

                }
                if(bulls == inputLength){

                    // Setting Dialog Message
                    alertDialog.setMessage(resources.getString(R.string.game_result_alert_win_message));

                    // Setting Icon to Dialog
//                    alertDialog.setIcon(R.drawable.delete);

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton(resources.getString(R.string.game_result_alert_positive_button_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.cancel();
                            resetGame();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton(R.string.game_result_alert_negative_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            //TODO:
                            Toast.makeText(getActivity().getApplicationContext(), "EXIT GAME", Toast.LENGTH_LONG).show();
                        }
                    });
                    alertDialog.show();

                }

            }

        });

    }

    private void resetGame(){
        //TODO:
        Toast.makeText(getActivity().getApplicationContext(), "RESET GAME", Toast.LENGTH_LONG).show();
        tries = resources.getInteger(R.integer.max_tries);
        tries_left.setText(new Integer(tries).toString());
        isNewGame = true;
    }

    private int getNumberToBeGuessed(){
        //TODO:
        return 1111;
    }

    /*@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



}
