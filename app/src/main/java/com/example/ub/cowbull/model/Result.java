package com.example.ub.cowbull.model;

/**
 * Created by ub on 14/12/14.
 */
public class Result {

    private int bulls;
    private int cows;

    public Result() {
    }

    public Result(int bulls, int cows) {
        this.bulls = bulls;
        this.cows = cows;
    }

    public int getBulls() {
        return bulls;
    }

    public void setBulls(int bulls) {
        this.bulls = bulls;
    }

    public int getCows() {
        return cows;
    }

    public void setCows(int cows) {
        this.cows = cows;
    }
}
